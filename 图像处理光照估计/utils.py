# _*_ coding : utf-8 _*_
# @Time : 2024-01-16 15:12
# @Author : haowen
# @File : utils
# @Project : pytest
import cv2
from math import exp
import numpy as np


def stretch_linear(img_arr, min_value=0.0, max_value=255.0):  ### 灰度线性拉伸
    return (img_arr - img_arr.min()) / (img_arr.max() - img_arr.min()) * (max_value - min_value) + min_value


def gaussian(window_size, sigma=1.5):  ### 定义高斯核
    gauss = np.array([exp(-(x - window_size // 2) ** 2 / float(2 * sigma ** 2)) for x in range(window_size)],
                     np.float64)
    return gauss / gauss.sum()


def create_window(H, W, sigma=1.5):  ### 创建窗口
    _1DH_window = np.expand_dims(gaussian(H, sigma), axis=-1)  ### 一维窗口
    _1DW_window = np.expand_dims(gaussian(W, sigma), axis=-1)  ### 一维窗口
    _2D_window = np.dot(_1DH_window, _1DW_window.T)  ### 二维窗口
    return _2D_window


def calc_ir(img_arr, patch_size=(47, 47), sigma=1.5):
    """输入:
       img_arr - 原图矩阵
       patch_size - 邻域大小
       sigma - 高斯核标准差
       输出:
       R_gray - 亮度分布灰度图
       R_heatmap - 亮度分布热力图
       R_fusion - 亮度分布热力图叠加原图结果
    """
    assert len(img_arr.shape) == 2

    """1. 求原图的灰度均值,rows和cols,并进行图像边界镜像填充
    """
    H, W = img_arr.shape
    # global_window = create_window(H, W, sigma)##
    local_window = create_window(patch_size[0], patch_size[1], sigma)
    img_mean = np.mean(img_arr)  # np.sum(img_arr * global_window)##
    pad_arr = cv2.copyMakeBorder(img_arr, patch_size[0] // 2, patch_size[0] // 2, patch_size[1] // 2,
                                 patch_size[1] // 2, cv2.BORDER_REFLECT)

    """2. 遍历原图所有像素点,对应边界填充图像上取以该像素点为中心的邻域求灰度均值(可用高斯均值)，得到亮度图像D
    """
    D = np.zeros((H, W), np.float64)
    for y in range(patch_size[0] // 2, patch_size[0] // 2 + H):
        for x in range(patch_size[1] // 2, patch_size[1] // 2 + W):
            start_y = y - patch_size[0] // 2
            start_x = x - patch_size[1] // 2
            end_y = start_y + patch_size[0]
            end_x = start_x + patch_size[1]
            D[start_y, start_x] = np.sum(pad_arr[start_y:end_y,
                                         start_x:end_x] * local_window)  # np.mean(pad_arr[start_y:end_y, start_x:end_x])##

    """3. 用图像D的每个元素减去原图的灰度均值
          得到亮度分布图像R并进行可视化
    """
    R = D - img_mean
    R_gray = stretch_linear(R).astype(np.uint8)
    R_heatmap = cv2.applyColorMap(R_gray, cv2.COLORMAP_JET)
    R_fusion = stretch_linear(np.float64(R_heatmap) + np.float64(np.expand_dims(img_arr, axis=-1)))

    return R_gray, R_heatmap, R_fusion
