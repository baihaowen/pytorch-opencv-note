# _*_ coding : utf-8 _*_
# @Time : 2024-01-16 15:12
# @Author : haowen
# @File : test
# @Project : pytest
import os
import cv2
from utils import calc_ir


ps_lst = [(5, 5), (11, 11), (23, 23), (47, 47), (95, 95), (191, 191)]

os.makedirs("low/gray", exist_ok=True)
os.makedirs("low/heatmap", exist_ok=True)
os.makedirs("low/fusion", exist_ok=True)

img_arr = cv2.imread("low.tif", 0)
for ps in ps_lst:
    R_gray, R_heatmap, R_fusion = calc_ir(img_arr, ps, max(img_arr.shape))
    cv2.imwrite("low/gray/ps" + str(ps[0]) + "x" + str(ps[1]) + ".png", R_gray)
    cv2.imwrite("low/heatmap/ps" + str(ps[0]) + "x" + str(ps[1]) + ".png", R_heatmap)
    cv2.imwrite("low/fusion/ps" + str(ps[0]) + "x" + str(ps[1]) + ".png", R_fusion)

os.makedirs("high/gray", exist_ok=True)
os.makedirs("high/heatmap", exist_ok=True)
os.makedirs("high/fusion", exist_ok=True)

img_arr = cv2.imread("high.tif", 0)
for ps in ps_lst:
    R_gray, R_heatmap, R_fusion = calc_ir(img_arr, ps, max(img_arr.shape))
    cv2.imwrite("high/gray/ps" + str(ps[0]) + "x" + str(ps[1]) + ".png", R_gray)
    cv2.imwrite("high/heatmap/ps" + str(ps[0]) + "x" + str(ps[1]) + ".png", R_heatmap)
    cv2.imwrite("high/fusion/ps" + str(ps[0]) + "x" + str(ps[1]) + ".png", R_fusion)