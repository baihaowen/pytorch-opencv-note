# _*_ coding : utf-8 _*_
# @Time : 2023-12-22 14:46
# @Author : haowen
# @File : 裁剪
# @Project : PytorchTest
import torch
# 进行值的约束
a = torch.rand(2, 2) * 10

print(a)
# 裁剪
a = a.clamp(2, 5)

print(a)
