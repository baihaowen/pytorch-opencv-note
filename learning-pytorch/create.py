# _*_ coding : utf-8 _*_
# @Time : 2023-12-21 14:41
# @Author : haowen
# @File : create
# @Project : PytorchTest
import torch

a = torch.normal(mean=torch.rand(5),std=torch.rand(5))
print(a)
print(a.type)


b = torch.Tensor(2,2).uniform_(-1,1)
print(b)
print(b.type)

# LongTensor
print(torch.arange(0,11,1))

print(torch.linspace(2,10,3))

# LongTensor
print(torch.randperm(10))

##################
import numpy as np
s = np.array([[2,3],[1,2]])
print(s)