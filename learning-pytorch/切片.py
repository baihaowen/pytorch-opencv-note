# _*_ coding : utf-8 _*_
# @Time : 2023-12-22 16:13
# @Author : haowen
# @File : 切片
# @Project : PytorchTest
import torch

a = torch.rand((3, 4))
print(a)
# 平均切
out = torch.chunk(a, 2, dim=1)
print(out[0], out[0].shape)
print(out[1], out[1].shape)


a = torch.rand((10, 4))
print(a)
# 剩下多少就是多少  常用！！！
out = torch.split(a, 3, dim=0)
print(len(out))
for t in out:
    print(t, t.shape)

out = torch.split(a, [1, 3, 6], dim=0)
for t in out:
    print(t, t.shape)


