# _*_ coding : utf-8 _*_
# @Time : 2024-01-04 14:08
# @Author : haowen
# @File : tensorboardx
# @Project : PytorchTest
# from tensorboardX import SummaryWriter
from torch.utils.tensorboard import SummaryWriter
writer = SummaryWriter("log")
for i in range(100):
    writer.add_scalar("a", i, global_step=i)
    writer.add_scalar("b", i ** 2, global_step=i)
writer.close()

