# _*_ coding : utf-8 _*_
# @Time : 2023-12-22 19:00
# @Author : haowen
# @File : 变形
# @Project : PytorchTest
import torch

a = torch.rand(2, 3)

print(a)
# 原先基础上依次改编为shape矩阵
out = torch.reshape(a, (3, 2))

print(out)

# 转置 行->列  列->行
print(torch.t(out))

# 把哪2个维度进行交换
print(torch.transpose(out, 0, 1))

a = torch.rand(1, 2, 3)
out = torch.transpose(a, 0, 1)
print(out)
print(out.shape)

# 去除维度大小为1的维度
out = torch.squeeze(a)
print(out)
print(out.shape)

# 对维度进行扩展
# -1指最后一个维度
out = torch.unsqueeze(a, -1)
print(out.shape)

# 去除某个维度
out = torch.unbind(a, dim=2)

print(out)

print(a)
# 按照给定维度翻转  图像增强
print(torch.flip(a, dims=[2, 1]))

print(a)
# 按照指定维度和翻转次数进行翻转 负数为瞬时针转，正数为逆时针
out = torch.rot90(a, -1, dims=[0, 2])
print(out)
print(out.shape)