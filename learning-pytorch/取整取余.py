# _*_ coding : utf-8 _*_
# @Time : 2023-12-21 16:54
# @Author : haowen
# @File : 取整取余
# @Project : PytorchTest
import torch

a = torch.rand(2, 2)
a = a * 10
print(a)

# 向下取整
print(torch.floor(a))
# 向上取整
print(torch.ceil(a))
# 四舍五入
print(torch.round(a))
# 裁剪,只取整数部分
print(torch.trunc(a))
# 只取小数部分
print(torch.frac(a))
# 取余
print(a % 2)
b = torch.tensor([[2, 3], [4, 5]],
                 dtype=torch.float)

# 余数的正负与被除数相同
print(torch.fmod(a, b))
# 余数与除数有相同的符号
print(torch.remainder(a, b))
