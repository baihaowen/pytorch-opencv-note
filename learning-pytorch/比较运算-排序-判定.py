# _*_ coding : utf-8 _*_
# @Time : 2023-12-21 17:08
# @Author : haowen
# @File : 比较运算-排序-判定
# @Project : PytorchTest
import torch
# 相同size
a = torch.rand(2, 3)
b = torch.rand(2, 3)

print(a)
print(b)

# 返回true和false的相同张量
print(torch.eq(a, b))
# 返回true或者false
print(torch.equal(a, b))

# >=
print(torch.ge(a, b))
# >
print(torch.gt(a, b))
#<=
print(torch.le(a, b))
#<
print(torch.lt(a, b))
#!=
print(torch.ne(a, b))

####

a = torch.tensor([[1, 4, 4, 3, 5],
                  [2, 3, 1, 3, 5]])
print(a.shape)

# descending=False 降序排序(默认)
# dim 排序的维度
print(torch.sort(a, dim=1,
                 descending=False))

##topk
# 返回最大值
a = torch.tensor([[2, 4, 3, 1, 5],
                  [2, 3, 5, 1, 4]])
print(a.shape)
# k=第几大的数
# dim返回维度
print(torch.topk(a, k=2, dim=1, largest=False))

# 返回最小的数
print(torch.kthvalue(a, k=2, dim=0))
print(torch.kthvalue(a, k=2, dim=1))

a = torch.rand(2, 3)
print(a)
print(a/0)
# 是否有界
print(torch.isfinite(a))
print(torch.isfinite(a/0))
#是否无界
print(torch.isinf(a/0))
#是否为nan
print(torch.isnan(a))

import numpy as np
a = torch.tensor([1, 2, np.nan])
print(torch.isnan(a))

a = torch.rand(2, 3)
print(a)
print(torch.topk(a, k=2, dim=1, largest=False))
print(torch.topk(a, k=2, dim=1, largest=True))


