# _*_ coding : utf-8 _*_
# @Time : 2023-12-22 14:04
# @Author : haowen
# @File : 线性代数运算 范数
# @Project : PytorchTest
import torch
# 作用
# 定义向量和向量之间的距离loss
# 参数约束 正则化等
a = torch.rand(2, 1)
b = torch.rand(2, 1)
print(a, b)
print(torch.dist(a, b, p = 1))
print(torch.dist(a, b, p = 2))
print(torch.dist(a, b, p = 3))

print(torch.norm(a))
print(torch.norm(a, p=3))
# p='fro'表示核范数
print(torch.norm(a, p='fro'))
