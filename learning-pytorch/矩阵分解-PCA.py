# _*_ coding : utf-8 _*_
# @Time : 2023-12-22 14:13
# @Author : haowen
# @File : 矩阵分解-PCA
# @Project : PytorchTest
# EVD分解
# 特征值分解 分解的为方阵且满秩（可对角化）
# 目标
# <1降低维度后同一维度方差大（特征越丰富）
# <2不同维度之间的相关性为0
# 采用：协方差矩阵