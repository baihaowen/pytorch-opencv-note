# _*_ coding : utf-8 _*_
# @Time : 2023-12-22 13:56
# @Author : haowen
# @File : 随机抽样
# @Project : PytorchTest
import torch
# 使用随机种子 约束随机抽样保证结果一致
torch.manual_seed(1)
mean = torch.rand(1, 2)
std  = torch.rand(1, 2)
print(torch.normal(mean, std))