# _*_ coding : utf-8 _*_
# @Time : 2023-12-22 19:26
# @Author : haowen
# @File : 填充
# @Project : PytorchTest
import torch

# 给tensor填充指定的数值
a = torch.full((2,3), 10)
print(a)

# 下面这个了解即可
# 扩展 频谱操作   傅里叶变换
# 可以完成对云信号的转换以及相关的一些拼图分析
