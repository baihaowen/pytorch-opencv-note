# _*_ coding : utf-8 _*_
# @Time : 2023-12-22 14:59
# @Author : haowen
# @File : 张量索引与数据筛选
# @Project : PytorchTest
import torch
#torch.where

a = torch.rand(4, 4)
b = torch.rand(4, 4)

print(a)
print(b)
# 满足a>0.5 输出a中的，否则输出b中的
out = torch.where(a > 0.5, a, b)

print(out)

#torch.index_select

print("torch.index_select")
a = torch.rand(4, 4)
print(a)
# 输出对应维度的索引上的值
out = torch.index_select(a, dim=0,
                   index=torch.tensor([0, 3, 2]))

print(out, out.shape)

#torch.gather

# .view改变shape
print("torch.gather")
a = torch.linspace(1, 16, 16).view(4, 4)

print(a)

# 拿到具体某个维度的值
out = torch.gather(a, dim=0,
             index=torch.tensor([[0, 1, 1, 1],
                                 [0, 1, 2, 2],
                                 [0, 1, 3, 3]]))
print(out)
print(out.shape)

#dim=0, out[i, j, k] = input[index[i, j, k], j, k]
#dim=1, out[i, j, k] = input[i, index[i, j, k], k]
#dim=2, out[i, j, k] = input[i, j, index[i, j, k]]

#torch.masked_index

# 选则择一些值
print("torch.masked_index")
a = torch.linspace(1, 16, 16).view(4, 4)
mask = torch.gt(a, 8)
print(a)
print(mask)
out = torch.masked_select(a, mask)
print(out)

#torch.take

# 取出指定索引，输出向量
print("torch.take")
a = torch.linspace(1, 16, 16).view(4, 4)

b = torch.take(a, index=torch.tensor([0, 15, 13, 10]))

print(b)

#torch.nonzero
# 输出非0元素的索引值坐标
print("torch.take")
a = torch.tensor([[0, 1, 2, 0], [2, 3, 0, 1]])
out = torch.nonzero(a)
print(out)
#稀疏表示




