# _*_ coding : utf-8 _*_
# @Time : 2023-12-22 19:35
# @Author : haowen
# @File : cv2等操作
# @Project : PytorchTest
import torch
import numpy as np
import cv2

data = cv2.imread("test.png")

print(data)

# 绘制当前图片 可视化
cv2.imshow("test1", data)

#必须加入，不然对话框不弹出
# cv2.waitKey(0)

# a = np.zeros([2, 2])

# 转换为numpy -> tensor
out = torch.from_numpy(data)

# out放到GPU运行
out = out.to(torch.device("cuda"))

print(out.is_cuda)

# 翻转
out = torch.flip(out, dims=[0])

out = out.to(torch.device("cpu"))

print(out.is_cuda)
# tensor -> numpy
data = out.numpy()

cv2.imshow("test2", data)

cv2.waitKey(0)