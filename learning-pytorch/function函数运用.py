# _*_ coding : utf-8 _*_
# @Time : 2023-12-25 18:42
# @Author : haowen
# @File : function函数运用
# @Project : PytorchTest
import torch

# 继承function
class line(torch.autograd.Function):
    # 静态函数
    @staticmethod
    def forward(ctx, w, x, b):
        #y = w*x +b

        # 缓存张量
        ctx.save_for_backward(w, x, b)
        return w * x + b

    # grad_out上一级梯度
    @staticmethod
    def backward(ctx, grad_out):
        # saved_tensors: 传给forward()的参数
        w, x, b = ctx.saved_tensors

        grad_w = grad_out * x
        grad_x = grad_out * w
        grad_b = grad_out

        return grad_w, grad_x, grad_b


# requires_grad开启后backward才会被调用
w = torch.rand(2, 2, requires_grad=True)
x = torch.rand(2, 2, requires_grad=True)
b = torch.rand(2, 2, requires_grad=True)

# 对重写的函数进行调用
out = line.apply(w, x, b)
# 进行反向传播
out.backward(torch.ones(2, 2))

print(w, x, b)
print(w.grad, x.grad, b.grad)

