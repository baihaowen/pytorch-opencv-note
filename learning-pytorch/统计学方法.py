# _*_ coding : utf-8 _*_
# @Time : 2023-12-21 21:21
# @Author : haowen
# @File : 统计学方法
# @Project : PytorchTest
import torch

a = torch.rand(2, 2)

print(a)
# 加上dim表示对某一维度求
# 平均
print(torch.mean(a, dim=0))
# 求和
print(torch.sum(a, dim=0))
# 求乘积
print(torch.prod(a, dim=0))
# 返回最大值的索引值
print(torch.argmax(a, dim=0))
# 返回最小值的索引值
print(torch.argmin(a, dim=0))

# 标准差 （用的多）
print(torch.std(a))
# 方差 （用的多）
print(torch.var(a))
# 中位数
print(torch.median(a))
# 众数
print(torch.mode(a))


a = torch.rand(2, 2) * 10
print(a)
# 直方图
# bin：统计多少个区间
# max和min可以自己定义，也可以用默认值0，0表示取出当前数据中的最大最小值
print(torch.histc(a, 6, 0, 0))


# 返回一个填充了随机整数的张量
a = torch.randint(0, 10, [10])
print(a)
# 频次
# 注：torch.bincount只能处理一维的tensor
print(torch.bincount(a))

# 统计某一类别样本的个数




