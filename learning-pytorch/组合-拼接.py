# _*_ coding : utf-8 _*_
# @Time : 2023-12-22 15:37
# @Author : haowen
# @File : 组合-拼接
# @Project : PytorchTest
import torch

a = torch.zeros((2, 4))
b = torch.ones((2, 4))

# 按照已经存在的维度拼接
# cat用的最多！！！！
out = torch.cat((a,b),dim=0)
print(out)

out = torch.cat((a,b),dim=1)
print(out)

#torch.stack
# 按照新维度拼接 2-》3   3-》4 ......
print("torch.stack")
a = torch.linspace(1, 6, 6).view(2, 3)
b = torch.linspace(7, 12, 6).view(2, 3)
print(a, b)
out = torch.stack((a, b), dim=1)
print(out)
print(out.shape)

# print(out[:, :, 0])
# print(out[:, :, 1])









