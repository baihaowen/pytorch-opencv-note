# _*_ coding : utf-8 _*_
# @Time : 2023-12-21 15:25
# @Author : haowen
# @File : 稀疏Tensor实践
# @Project : PytorchTest
import torch

# dev = torch.device("cpu")
dev = torch.device("cuda:0")
a = torch.tensor([2,2], dtype=torch.float32,device=dev)
print(a)

# 稀疏张量定义↓

# 非0元素坐标
# (0,0) (1,1) (2,2)
i = torch.tensor([[0,1,2],[0,1,2]])
# 非0元素具体的值
v = torch.tensor([1,2,3])
# 当前tensor的size
# .to_dense 转换为稠密的张量看看
b = torch.sparse_coo_tensor(i,v,[4,4],dtype=torch.float32,device=dev).to_dense()
print(b)

