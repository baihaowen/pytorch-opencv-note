# _*_ coding : utf-8 _*_
# @Time : 2023-12-21 16:44
# @Author : haowen
# @File : 广播机制
# @Project : PytorchTest

# 至少有1维度
# 满足右对齐
# 四则运算都可
import torch

# 2 * 3
# 1 * 3  -pass
a = torch.rand(2,3)
b = torch.rand(3)

print(a)
print(b)
c = torch.add(a, b)
print(c)