# _*_ coding : utf-8 _*_
# @Time : 2024-01-22 15:39
# @Author : haowen
# @File : mask处理
# @Project : pytest
import cv2
import numpy as np
from matplotlib import pyplot as plt

img =cv2.imread("test.jpg", 0)
mask = np.zeros(img.shape[:2], np.uint8)
mask[50:100, 50:100] = 255

masked_img = cv2.bitwise_and(img,img,mask=mask)

hist = cv2.calcHist([img], [0], mask, [256], [0, 256])
plt.plot(hist, color = 'r')
plt.xlim([0, 256])
plt.show()
# cv2.imshow("mask",masked_img)
# cv2.waitKey()
# cv2.destroyAllWindows()