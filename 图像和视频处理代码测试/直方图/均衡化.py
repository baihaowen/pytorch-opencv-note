# _*_ coding : utf-8 _*_
# @Time : 2024-01-22 16:29
# @Author : haowen
# @File : 均衡化
# @Project : pytest
import cv2
from matplotlib import pyplot as plt

img = cv2.imread('test.jpg', 0)
# 均衡化
equ = cv2.equalizeHist(img)

plt.hist(equ.ravel(),256)
plt.xlim([0, 256])
plt.show()
