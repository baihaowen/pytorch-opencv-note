# _*_ coding : utf-8 _*_
# @Time : 2024-01-22 15:23
# @Author : haowen
# @File : do
# @Project : pytest
import cv2
from matplotlib import pyplot as plt

img = cv2.imread('test.jpg', 0)
hist = cv2.calcHist([img], [0], None, [256], [0, 256])
# hist.shape()

plt.plot(hist, color = 'r')
plt.xlim([0, 256])
plt.show()
