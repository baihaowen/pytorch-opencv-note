# _*_ coding : utf-8 _*_
# @Time : 2024-01-22 16:40
# @Author : haowen
# @File : 自适应直方图均衡化
# @Project : pytest
import cv2
import numpy as np

img = cv2.imread('test.jpg', 0)
# 自适应均衡化
clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))

res_clahe= clahe.apply(img)
res = np.hstack([img, res_clahe])
cv2.imshow("res",res)
cv2.waitKey()
cv2.destroyAllWindows()

