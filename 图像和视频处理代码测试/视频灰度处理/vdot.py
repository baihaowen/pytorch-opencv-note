# _*_ coding : utf-8 _*_
# @Time : 2024-01-17 16:12
# @Author : haowen
# @File : vdot
# @Project : pytest
import cv2
import os

vc = cv2.VideoCapture('test.mp4')

if vc.isOpened():
    open, frame = vc.read()
else:
    open = False

os.makedirs("grayAll", exist_ok=True)
i = 0
while open:
    ret, frame = vc.read()
    if frame is None:
        break
    if ret == True:
        i += 1
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        cv2.imwrite("grayAll/ps" + str(i) + ".png",gray)
        # cv2.imshow('result', gray)
        # if cv2.waitKey(100) & 0xFF == 27:
        #     break

# 释放硬件资源
vc.release()
cv2.destroyAllWindows()
