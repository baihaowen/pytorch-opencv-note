# _*_ coding : utf-8 _*_
# @Time : 2024-01-21 16:49
# @Author : haowen
# @File : do
# @Project : pytest
import cv2

img = cv2.imread("test.png")
img = cv2.resize(img,(500,400))
# 转换为灰度图
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

# 阈值处理,让图像颜色只有2种颜色  提高准确性
ret, thresh = cv2.threshold(gray, 127, 255, cv2.THRESH_BINARY)
# 进行轮廓处理
# cv2.findContours返回两个值
# contours 轮廓点（是个列表）
# hierarchy 层数（用不到）
contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
# 轮廓描边后会影响原图,所以我们定义一个临时的图片
temp_img = img.copy()
# 进行轮廓描边
# contours 获取到的轮廓点
# -1 表示的画出所有的轮廓，eg:0就表示我们列表中第一个轮廓
# (0, 0, 255) 表示我们用红色线条来绘画 bgr
# 2 表示线条粗细
res = cv2.drawContours(temp_img, contours, -1, (0, 0, 255), 2)

cv2.imshow("res",res)
cv2.waitKey()
cv2.destroyAllWindows()

