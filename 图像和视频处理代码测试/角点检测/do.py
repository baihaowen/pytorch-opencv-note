# _*_ coding : utf-8 _*_
# @Time : 2024-01-29 16:39
# @Author : haowen
# @File : do
# @Project : pytest
import cv2
import numpy as np

img = cv2.imread('test.jpg')
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
# gray = np.float32(gray)
dst = cv2.cornerHarris(gray, 2, 3, 0.04)

img[dst>0.01*dst.max()]=[0,0,255]
cv2.imshow('img',img)
cv2.waitKey(0)
cv2.destroyAllWindows()