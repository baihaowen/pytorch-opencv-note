# _*_ coding : utf-8 _*_
# @Time : 2024-01-21 21:00
# @Author : haowen
# @File : found
# @Project : pytest
import cv2
from matplotlib import pyplot as plt
img = cv2.imread("hui.jpg",0)
template = cv2.imread("fihui.jpg",0)
# 获取模板的高和宽
h,  w = template.shape[:2]

# 模板匹配
res = cv2.matchTemplate(img, template, 3)
# 定位
# min_val 最小值
# min_loc 最小值坐标
min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)

top_left = max_loc
bottom_right = (top_left[0] + w, top_left[1] + h)

fondsite = cv2.rectangle(img, top_left, bottom_right, 255, 2)

cv2.imshow("fondsite",fondsite)
cv2.waitKey()
cv2.destroyAllWindows()
