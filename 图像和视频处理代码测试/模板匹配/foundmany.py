# _*_ coding : utf-8 _*_
# @Time : 2024-01-21 21:49
# @Author : haowen
# @File : foundmany
# @Project : pytest
import cv2
import numpy as np
from matplotlib import pyplot as plt
img = cv2.imread("hui.jpg", 0)
template = cv2.imread("eye.jpg", 0)
# 获取模板的高和宽
h,  w = template.shape[:2]

# 模板匹配
res = cv2.matchTemplate(img, template, 3)
# 取匹配程度大于%97的坐标
# 定义的阈值 threshold
threshold = 0.97
# np.where返回的坐标值(x,y)是(h,w)
loc = np.where(res >= threshold)
for top_left in zip(*loc[::-1]):
    bottom_right = (top_left[0] + w, top_left[1] + h)
    cv2.rectangle(img, top_left, bottom_right, 255, 1)
cv2.imshow('img', img)
cv2.waitKey()
cv2.destroyAllWindows()