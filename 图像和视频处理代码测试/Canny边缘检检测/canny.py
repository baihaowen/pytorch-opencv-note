# _*_ coding : utf-8 _*_
# @Time : 2024-01-20 19:12
# @Author : haowen
# @File : canny
# @Project : pytest
import cv2
import numpy  as np

img=cv2.imread("test.png",cv2.IMREAD_GRAYSCALE)
# 修改图像大小
show = cv2.resize(img,(300,300))

v1=cv2.Canny(show,120,250)
v2=cv2.Canny(show,50,100)

# 连接图像
res = np.hstack((v1,v2))
cv2.imshow("img",res)
cv2.waitKey()
cv2.destroyAllWindows()