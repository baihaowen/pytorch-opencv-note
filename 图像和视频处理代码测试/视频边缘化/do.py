# _*_ coding : utf-8 _*_
# @Time : 2024-01-20 17:22
# @Author : haowen
# @File : do
# @Project : pytest
import cv2

vc = cv2.VideoCapture('test.mp4')

if vc.isOpened():
    open, frame = vc.read()
else:
    open = False

i = 0
while open:
    ret, frame = vc.read()
    if frame is None:
        break
    if ret == True:
        i += 1
        # 转换为灰度图
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        # 使用Sobel进行边缘处理
        sobelx = cv2.Sobel(gray, cv2.CV_64F, 1, 0, ksize=3)
        sobelx = cv2.convertScaleAbs(sobelx)

        sobely = cv2.Sobel(gray, cv2.CV_64F, 0, 1, ksize=3)
        sobely = cv2.convertScaleAbs(sobely)
        # 合起来
        sobelxy = cv2.addWeighted(sobelx, 0.5, sobely, 0.5, 0)
        cv2.imshow('result', sobelxy)
        # 0.1s 0xFF表示键盘上的Esc键
        if cv2.waitKey(100) & 0xFF == 27:
            break

# 释放硬件资源
vc.release()
# 清除所有窗口
cv2.destroyAllWindows()