# _*_ coding : utf-8 _*_
# @Time : 2024-01-30 15:19
# @Author : haowen
# @File : test
# @Project : pytest
import numpy as np
import matplotlib.pyplot as plt
from scipy.ndimage import gaussian_filter
import cv2

# 生成一个示例图像
image_size = 100
image = np.zeros((image_size, image_size))
# image = cv2.imread('test.png')
cv2.imshow("image",image)
cv2.waitKey(0)

# 在图像中心创建一个高亮区域
center_x, center_y = image_size // 2, image_size // 2
image[center_x - 10:center_x + 10, center_y - 10:center_y + 10] = 1.0
cv2.imshow("image",image)
cv2.waitKey(0)

# 调整sigma来得到不同效果
sigma = 5
smoothed_image = gaussian_filter(image, sigma=sigma)
cv2.imshow("smoothed_image",smoothed_image)
cv2.waitKey(0)
