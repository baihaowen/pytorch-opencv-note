# _*_ coding : utf-8 _*_
# @Time : 2024-01-30 15:10
# @Author : haowen
# @File : do
# @Project : pytest
import numpy as np
import matplotlib.pyplot as plt
from scipy.ndimage import gaussian_filter

# 生成一个示例图像
image_size = 100
image = np.zeros((image_size, image_size))

# 在图像中心创建一个高亮区域
center_x, center_y = image_size // 2, image_size // 2
image[center_x - 10:center_x + 10, center_y - 10:center_y + 10] = 1.0

# 对图像进行高斯平滑处理
sigma = 5
smoothed_image = gaussian_filter(image, sigma=sigma)

# 绘制原始图像
plt.subplot(1, 2, 1)
plt.title('Original Image')
plt.imshow(image, cmap='viridis')
plt.colorbar()

# 绘制经过高斯平滑处理后的图像
plt.subplot(1, 2, 2)
plt.title('Smoothed Image (Gaussian Filter)')
plt.imshow(smoothed_image, cmap='viridis')
plt.colorbar()

plt.show()
