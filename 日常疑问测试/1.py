# _*_ coding : utf-8 _*_
# @Time : 2024-01-07 19:46
# @Author : haowen
# @File : 1
# @Project : PytorchTest

import torch
import torch.nn.functional as F

a = torch.rand(2,3)
print(a)
b = a.size(0)
print(b)

c = F.log_softmax(a,dim=0)
d = F.softmax(a,dim=0)
print(c)
print(d)
e = a.view(3,-1)
print(e)
