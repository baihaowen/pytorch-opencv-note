# _*_ coding : utf-8 _*_
# @Time : 2024-01-16 15:23
# @Author : haowen
# @File : test
# @Project : pytest
import cv2
import numpy as np
def stretch_linear(img_arr, min_value=0.0, max_value=255.0):  ### 灰度线性拉伸
    return (img_arr - img_arr.min()) / (img_arr.max() - img_arr.min()) * (max_value - min_value) + min_value

image = cv2.imread("test.jpg")
add_image = cv2.add(image,image)
hotimage = cv2.applyColorMap(image, cv2.COLORMAP_JET)
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
linear = stretch_linear(gray).astype(np.uint8)
fusion = stretch_linear(hotimage + image)

cv2.imshow("image", image)
cv2.imshow("add_image", image)
cv2.imshow("hotimage", hotimage)
cv2.imshow("gray", gray)
cv2.imshow("linear", linear)
cv2.imshow("fusion", fusion)

cv2.waitKey()