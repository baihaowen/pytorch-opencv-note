# _*_ coding : utf-8 _*_
# @Time : 2024-01-07 20:05
# @Author : haowen
# @File : 2
# @Project : PytorchTest
import torch
import torch.nn as nn
x = torch.rand(3,5,5)
print(x)
print(x.shape)

conv = nn.Conv2d(3,6,kernel_size=3,stride=1,padding=1)
y = conv(x)
print(y)
print(y.shape)

reul = nn.ReLU()
E = reul(y)
print(E)
print(E.shape)

pooling = nn.MaxPool2d(kernel_size=2, stride=2)
F = pooling(E)
print(F)
print(F.shape)

