# _*_ coding : utf-8 _*_
# @Time : 2024-01-18 13:30
# @Author : haowen
# @File : 6-1
# @Project : pytest
import torch.nn as nn
import torch
x = torch.rand(3,5,5)
print(x.shape)
# 卷积
conv = nn.Conv2d(3,3,kernel_size=3,stride=1)
y= conv(x)
print(y.shape)