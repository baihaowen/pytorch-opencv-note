# _*_ coding : utf-8 _*_
# @Time : 2024-01-18 13:12
# @Author : haowen
# @File : 6
# @Project : pytest
import cv2


img = cv2.imread("te.png")
print(img.shape)
# print(img[:5,:,0])


img1 = cv2.blur(img,(3,3))
print(img1.shape)
# print(img1[:5,:,0])

img2 = cv2.GaussianBlur(img,(3,3),1)
print(img2.shape)
# print(img2[:5,:,0])

img3 = cv2.medianBlur(img, 3)
print(img3.shape)
# print(img3[:5,:,0])