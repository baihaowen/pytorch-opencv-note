# _*_ coding : utf-8 _*_
# @Time : 2024-01-24 19:56
# @Author : haowen
# @File : 8
# @Project : pytest

# 假设a里面的()分别对应的x,y,w,h 即 (x,y,w,h)
a = [(2,3,1,2),(4,1,2,2),(1,6,2,1)]
# a:传入的列表
# key 排序的数据 key=lambda x:x[n] 是固定写法,里面的n代表你按照()中第几个数据的值排序
# eg：我们这里是x:x[0]表示我们按x排序, 如果改成x:x[1]则按y排序
# reverse: 排序是升序还是降序,True代表降序
new_a = sorted(a,key=lambda x:x[1], reverse=True)
print(new_a)

b = (2,4,5,1)
new_b = sorted(b,reverse=True)
print(new_b)


