# _*_ coding : utf-8 _*_
# @Time : 2024-01-24 17:22
# @Author : haowen
# @File : 7
# @Project : pytest
import argparse

# 设置参数
ap = argparse.ArgumentParser()
ap.add_argument("-n1", "--number1", required=True, help="set number1", type=int)
ap.add_argument("-n2", "--number2", required=True, help="set number1", type=int)
# vars函数:返回该对象的属性和属性值组成的字典
args = vars(ap.parse_args())
sum = args["number1"] + args["number2"]
print(sum)