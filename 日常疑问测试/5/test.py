# _*_ coding : utf-8 _*_
# @Time : 2024-01-16 17:20
# @Author : haowen
# @File : test
# @Project : pytest
import cv2
import matplotlib.pyplot as plt
image = cv2.imread("test.jpg")
# 彩色图片->灰色图片
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

# cv2.calcHist([图像], [通道数(灰度图是0)]， mask(掩码图像,为none即可), histSize(bines数量), [ranges(像素范围)])
# histSize(bines数量) 表示的是横坐标尺寸
hist1 = cv2.calcHist([gray], [0], None, [256], [0, 255])
# hist2 = cv2.calcHist([image], [2], None, [256], [0, 256])
plt.plot(hist1, color='b', label='hist1')
# plt.plot(hist2, color='r',label='hist2')
plt.legend()
plt.show()
# print(gray)
