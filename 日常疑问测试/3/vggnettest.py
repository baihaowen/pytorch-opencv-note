# _*_ coding : utf-8 _*_
# @Time : 2024-01-14 16:11
# @Author : haowen
# @File : vggnettest
# @Project : PytorchTest
from torchsummary import summary
from vggnet import VGGNet

myNet = VGGNet().cuda()
summary(myNet, (3, 28, 28))
# print(myNet)