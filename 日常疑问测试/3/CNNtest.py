# _*_ coding : utf-8 _*_
# @Time : 2024-01-14 16:10
# @Author : haowen
# @File : CNNtest
# @Project : PytorchTest
from torchsummary import summary
from CNN import CNN

myNet = CNN().cuda()
summary(myNet, (1, 28, 28))
# print(myNet)